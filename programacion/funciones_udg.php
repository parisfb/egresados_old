<?php

	// Funci�n para prevenir que robots inform�ticos tomen las direcciones de correo electr�nico del sitio
	function proteger_correo( &$contenido )
	{
		if ( preg_match( '/UDG_CORREOS/i', $contenido ) ) 
		{
			while( preg_match( '/{UDG_CORREOS}(.*){\/UDG_CORREOS}/iusU', $contenido, $found ) )
			{
				preg_match( '/{CORREO}(.*){\/CORREO}/iusU', $found[1], $CORREO );
				preg_match( '/{TITULO}(.*){\/TITULO}/iusU', $found[1], $TITULO );
				$idcorreo	= 'udg_correo_anti_' . str_replace( ' ', '', str_replace( '.', '', str_replace( '@', '', $CORREO[1] ) ) );
				$contenidoenncriptado	= str_replace( '@', '|', $CORREO[1] );
				$html = '<div id="' . $idcorreo . '" class="udg_css_correo">email</div>
				<script>
					/*<![CDATA[*/
					$(window).load( function () {
						var texto_' . $idcorreo . '	= "' . $contenidoenncriptado . '";
						texto_' . $idcorreo . '		= texto_' . $idcorreo . '.replace(/\|/gi, "@");
						document.getElementById("' . $idcorreo . '").innerHTML	=\'<a href="mailto:\'+texto_' . $idcorreo . '+\'?subject=' . $TITULO[1] . '">\' + texto_' . $idcorreo . ' + \'</a>\';
					} );
					/*]]>*/
				</script>';
				$contenido = str_replace( $found, $html, $contenido );
			}
		}
	}
	
	
	
	// Funci�n para devolver el id de un t�rmino taxonom�a en base al alias de la URL
	function regresa_tid( $url_alias )
	{
		// Hacer la consulta para traer el tid
		$sql_url = "SELECT url.src
				FROM url_alias AS url
				WHERE url.dst = '" . $url_alias . "'
				LIMIT 1";
		$res_url = db_query( $sql_url );
		
		if( $alias = db_fetch_object( $res_url ) )
		{
			// Separar para obtener el term id
			$arreglo_url = explode( '/', $alias -> src );
					
			// Obtener el tid
			return $arreglo_url[ count( $arreglo_url ) - 1 ];
		}
		
		return '';
	}
	
	
	
	// Funci�n para devolver los id's de nodos que un rol determinado puede editar.
	// Es �til para las vistas en la nueva actualizaci�n para el manejo de banners, slideshows, etc.
	function regresa_nodos_por_rol( $tipo_contenido )
	{
		global $user;
		
		$band_todos = false;
		$ids_nodos = '';
		
		
		foreach( array_keys( $user -> roles ) as $rid )
		{
			foreach( $tipo_contenido as $tipo_aux ) 
			{
				$query = "SELECT pid FROM permission
						WHERE rid = '$rid'
						AND ( perm LIKE '%administer content%' OR perm LIKE 'delete any $tipo_aux content' )";
				$result = db_query( $query );
				
				if( $row = db_fetch_array( $result ) ) {
					$band_todos = true;
				}
				
				$ridQuery .= "gid = $rid OR ";
			}
		}
		
		
		if( $band_todos ) 
		{
			$ids_nodos = 'all';
		}
		
		else
		{
			$ridQuery = rtrim($ridQuery, ' OR '); 
			
			$query = "SELECT DISTINCT nid FROM node_access 
				WHERE realm = 'content_access_rid'
				AND grant_update = '1'
				AND $ridQuery";
			
			$result = db_query( $query );
			
			while( $row = db_fetch_array( $result ) ){
				$ids_nodos .= $row['nid'] . '+';
			}
			
			$ids_nodos = rtrim($ids_nodos, ' + '); 
		}
		
		return $ids_nodos;
	}
	
	
?>
