<?php

// $Id: udgsitemap.ajax.inc $

/**
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * UDG Sitemap 6.2
 *
 * Por: Jorge Ramos
 * Julio 2010
 *
 * Desarrollo de la interfaz de llamadas por medio de AJAX de UDG Sitemap.
 *
 **/





	/**
	* Recibe las peticiones por medio de AJAX desde la interfaz
	* principal del administrador
	**/
	function udgsitemap_ajax() 
	{
		
		// Eliminar si existe la variable
		if( isset( $_POST[ 'id_borrar' ] ) ) 
		{
			$query = "DELETE FROM udgsitemap_extras WHERE id = '%d' ";
			$query_result =  db_query( $query, $_POST[ 'id_borrar' ] );
			echo 'Borrado';
		}
		
		// Hacer la consulta para editar
		else if( isset( $_POST[ 'id_editar' ] ) ) 
		{
			$query = "SELECT * FROM udgsitemap_extras WHERE id = '%d' ";
			$query_result =  db_query( $query, $_POST[ 'id_editar' ] );
			
			// Traer datos de la base
			while( $res_extras = db_fetch_object( $query_result ) ) {
				echo $res_extras->id_menu . '$' . $res_extras->titulo . '$' . $res_extras->liga;
			}
			
		}
		
		// Hacer consulta para traer las taxonom�as
		else if( isset( $_POST[ 'menu_id' ] ) && isset( $_POST[ 'id_categoria' ] ) && isset( $_POST[ 'tipo_contenido' ] ) )
		{
			// Inicializar variables
			$lista_taxonomias = '<option value="-1"';
			
			// Verificar si ese valor ser� "selected"
			if( $_POST[ 'id_categoria' ] == "-1" ) {
				$lista_taxonomias .= ' selected="selected" ';
			}
			
			$lista_taxonomias .= '>Ninguna</option>';
			
			$lista_taxonomias .= '<option value="0"';
			
			// Verificar si ese valor ser� "selected"
			if( $_POST[ 'id_categoria' ] == "0" ) {
				$lista_taxonomias .= ' selected="selected" ';
			}
			
			$lista_taxonomias .= '>Todas</option>';
			
			
			$query_tax = "SELECT v.vid AS vid, v.name AS name
					FROM vocabulary AS v, vocabulary_node_types AS vn
					WHERE v.vid = vn.vid
					AND vn.type = '%s' ";
			
			$query_tax_res = db_query( $query_tax, $_POST[ 'tipo_contenido' ] );
			while( $elemento_tax = db_fetch_object( $query_tax_res ) )
			{
				// Hacer opciones para vocabulario
				$lista_taxonomias .= '<option value="' . $elemento_tax->vid . '"';
				
				// Verificar si ese valor ser� "selected"
				if( $elemento_tax->vid == $_POST[ 'id_categoria' ] ) {
					$lista_taxonomias .= ' selected="selected" ';
				}
				
				$lista_taxonomias .= '>' . $elemento_tax->name . '</option>';
			}
			
			echo $lista_taxonomias;
		}
		
		
		// Hacer consulta para traer los t�rminos
		else if( isset( $_POST[ 'menu_id' ] ) && isset( $_POST[ 'id_categoria' ] )  )
		{
			// Inicializar variables
			$lista_terminos = '<option value="-1">Ninguno</option><option value="0">Todos</option>';
			
			// Lllamar a la funci�n para traer usuarios por ajax
			$lista_terminos .= udgsitemap_regresa_terminos_ajax( $_POST[ 'id_categoria' ], 0, '', 0 );
			
			echo $lista_terminos;
			
		}
		
		
		// Hacer consulta para traer los roles
		else if( isset( $_POST[ 'menu_id' ] ) && isset( $_POST[ 'id_rol' ] )  && isset( $_POST[ 'cambia_roles' ] )  )
		{
			// Inicializar variables
			$lista_roles = '<option value="-1"';
			
			// Verificar si ese valor ser� "selected"
			if( $_POST[ 'id_rol' ] == "-1" ) {
				$lista_roles .= ' selected="selected" ';
			}
			
			$lista_roles .= '>Ninguno</option>';
			
			$lista_roles .= '<option value="0"';
			
			// Verificar si ese valor ser� "selected"
			if( $_POST[ 'id_rol' ] == "0" ) {
				$lista_roles .= ' selected="selected" ';
			}
			
			$lista_roles .= '>Todos</option>';
			
			// En la consulta quitar a anonymous y authenticated user de los roles
			$query_roles = "SELECT r.rid, r.name FROM role AS r
					WHERE r.rid <> '1'
					AND r.rid <> '2'
					ORDER BY r.rid";
			
			$query_roles_res = db_query( $query_roles );
			while( $elemento_roles = db_fetch_object( $query_roles_res ) )
			{
				// Hacer opciones para rol
				$lista_roles .= '<option value="' . $elemento_roles->rid . '"';
				
				// Verificar si ese valor ser� "selected"
				if( $elemento_roles->rid == $_POST[ 'id_rol' ] ) {
					$lista_roles .= ' selected="selected" ';
				}
				
				$lista_roles .= '>' . $elemento_roles->name . '</option>';
				
			}
			
			echo $lista_roles;
			
		}
		
		
		// Hacer consulta para traer los usuarios
		else if( isset( $_POST[ 'menu_id' ] ) && isset( $_POST[ 'id_rol' ] )  && isset( $_POST[ 'id_usuario' ] )  )
		{
			// Inicializar variables
			$lista_usuarios = '<option value="-1">Ninguno</option><option value="0">Todos</option>';
			
			// Lllamar a la funci�n para traer usuarios por ajax
			$lista_usuarios .= udgsitemap_regresa_usuarios_ajax( $_POST[ 'id_rol' ], $_POST[ 'id_usuario' ] );
			
			echo $lista_usuarios;
			
		}
		
		
	} // function udgsitemap_ajax() 
	
	
	
	
	
	/**
	* Devuelve listado de opciones de select con los tipos de contenido y sus taxonom�as.
	* Es llamada mediante el uso de AJAX.
	* @param integer $id_vocabulario: Arreglo
	* @param integer $padre_term: Id del t�rmino padre
	* @param integer $prefijo: Prefijo para mostrar la profundidad del elemento
	* @param integer $contador_prof: Contador de profundidad para cortar el ciclo recursivo
	* @return string $resultado: Lista de los t�rminos
	**/
	function udgsitemap_regresa_terminos_ajax( $id_vocabulario, $padre_term, $prefijo, $contador_prof = 0 ) 
	{
		
		// Variable del resultado
		$resultado = '';
		
		// Si el contador de profundidad es mayor a 2, regresar valor vac�o
		if( $contador_prof > 2 ) {
			return $resultado;
		}
		
		// Obtener todos los t�rminos de ese vocabulario que no tengan padre
		$query = "SELECT td.tid, td.name
				FROM term_data AS td, term_hierarchy AS th
				WHERE td.vid = '%d'
				AND td.tid = th.tid
				AND th.parent = '%s'
				ORDER BY td.weight";
		
		$query_result =  db_query( $query, $id_vocabulario, $padre_term );
		while( $elemento = db_fetch_object( $query_result ) ) 
		{
			// Agregar elemento al resultado
			$resultado .= '<option value="' . $elemento->tid . '">' . $prefijo . ' ' . $elemento->name . '</option>';
			
			// Hacer una consulta en la tabla de jerarqu�as para saber si ese t�rmino tiene hijos
			$query_hijos = "SELECT COUNT( DISTINCT tid ) AS total_hijos FROM term_hierarchy AS th
				WHERE th.parent = '%d' ";
			
			$query_hijos_res = db_query( $query_hijos, $elemento->tid );
			if( $elemento_hijos = db_fetch_object( $query_hijos_res ) ){
				
				// Si hay hijos, entonces hacer llamada recursiva a la funci�n
				if( $elemento_hijos->total_hijos ){
					$resultado .= udgsitemap_regresa_terminos_ajax( $id_vocabulario, $elemento->tid, $prefijo . '--', ( $contador_prof + 1 ) );
				}
				
			}
			
			
		}
		
		return $resultado;
		
	} // function udgsitemap_regresa_terminos_ajax()
	
	
	
	
	
	/**
	* Devuelve listado de opciones de select con los tipos de contenido y sus taxonom�as.
	* Es llamada mediante el uso de AJAX.
	* @param integer $id_rol: Id del rol de usuario
	* @param integer $id_usuario: Id de usuario
	* @return string $resultado: Lista de los usuarios
	**/
	function udgsitemap_regresa_usuarios_ajax( $id_rol, $id_usuario )
	{
		
		// Variable del resultado
		$resultado = '';
		
		// Obtener todos los t�rminos de ese vocabulario que no tengan padre
		$query = "SELECT u.uid, u.name
				FROM users AS u, users_roles AS ur
				WHERE u.uid = ur.uid";
		
		// Si hay que traer el resultado de un rol en espec�fico
		if( $id_rol != "-1" && $id_rol != "0" ) {
			$query .= " AND ur.rid = '%d' ";
		}
		
		$query .= " ORDER BY u.uid ";
		
		$query_result =  db_query( $query, $id_rol );
		while( $elemento = db_fetch_object( $query_result ) ) 
		{
			// Agregar elemento al resultado
			$resultado .= '<option value="' . $elemento->uid . '"';
			
			// Verificar si ese valor ser� "selected"
			if( $elemento->uid == $id_usuario ) {
				$lista_taxonomias .= ' selected="selected" ';
			}
			
			$resultado .= '>' . $elemento->name . '</option>';
		}
		
		return $resultado;
		
	} // udgsitemap_regresa_usuarios_ajax()
	
	
	
	