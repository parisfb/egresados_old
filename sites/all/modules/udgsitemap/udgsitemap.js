/**
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * UDG Sitemap 6.2
 *
 * Por: Jorge Ramos
 * Julio 2010
 *
 * Archivo de funciones javascript para el módulo de udgsitemap.
 * Las funciones utilizan jQuery.
 *
 *
**/




/**
* Función para inicializar, trayendo los valores por default
**/
function inicializar( lista_ids_menu )
{
	// Separar los valores de los ids de menú en un arreglo
	arreglo_ids = lista_ids_menu.split( "," );
	
	// Recorrer el arreglo
	for( i= 0; i < arreglo_ids.length - 1; i++ )
	{
		// Subdividir el arreglo
		arreglo_aux = arreglo_ids[i].split( "/" );
		
		// Cambiar categorías
		cambia_select_categoria( arreglo_aux[0], $( "#edit-categoria-" + arreglo_aux[0] ).val(), $( "#edit-tipo-contenido-" + arreglo_aux[0] ).val() );
		document.getElementById( "edit-termino-" + arreglo_aux[0] ).value = arreglo_aux[1];
		
		// Cambiar usuarios
		cambia_select_rol( arreglo_aux[0], $( "#edit-rol-" + arreglo_aux[0] ).val() );
		document.getElementById( "edit-usuario-" + arreglo_aux[0] ).value = arreglo_aux[2];
	}
}





/**
* Función para enviar el formulario principal
**/
function envia_formulario() 
{	
	$(".etq_form_item.menu_id").each( 
			function()
			{
				// Obtener el valor del select
				mid = $( this ).find( "select" ).val();
				
				// Inicializar variable auxiliar
				valor_aux = '';
				
				// Si el valor es diferente a -1
				if( mid != "-1" )
				{
					// Asignar el valor del id del menú al auxiliar
					valor_aux = mid;
					
					// Obtener los valores de los demás campos y concatenarlos al valor del id del menú
					valor_aux += "/" + $( "#edit-tipo-contenido-" + mid ).val();
					valor_aux += "/" + $( "#edit-categoria-" + mid ).val();
					valor_aux += "/" + $( "#edit-termino-" + mid ).val();
					valor_aux += "/" + $( "#edit-rol-" + mid ).val();
					valor_aux += "/" + $( "#edit-usuario-" + mid ).val();
					valor_aux += "/" + $( "#edit-tiene-hijos-" + mid ).val();
					
					// Asignar el valor al mid
					$( "#edit-valor-" + mid ).val( valor_aux );
				}
				
			}
	);
	
}





/**
* Función para cambiar las opciones de los selects
* de acuerdo al tipo de contenido
**/
function cambia_selects_tipo_contenido( menu_id, valor, id_categoria )
{
	// Si no se ha seleccionado tipo de contenido
	if( valor == '0' )
	{		
		// Bloquear los otros campos
		$( "#edit-tipo-busqueda-" + menu_id ).attr( 'disabled', 'disabled' );
		$( "#edit-categoria-" + menu_id ).attr( 'disabled', 'disabled' );
		$( "#edit-termino-" + menu_id ).attr( 'disabled', 'disabled' );
		$( "#edit-rol-" + menu_id ).attr( 'disabled', 'disabled' );
		$( "#edit-usuario-" + menu_id ).attr( 'disabled', 'disabled' );
	}
	
	// En caso contrario
	else
	{
		// Bloquear los otros campos
		$( "#edit-tipo-busqueda-" + menu_id ).attr( 'disabled', '' );
		$( "#edit-categoria-" + menu_id ).attr( 'disabled', '' );
		$( "#edit-termino-" + menu_id ).attr( 'disabled', '' );
		$( "#edit-rol-" + menu_id ).attr( 'disabled', '' );
		$( "#edit-usuario-" + menu_id ).attr( 'disabled', '' );
		
		// Rehacer los valores de los selects que dependen del tipo de contenido
		cambia_select_categoria( menu_id, id_categoria, valor );
	}
	
}





/**
* Función para cambiar las opciones de los selects
* de acuerdo a la categoría
**/
function cambia_select_categoria( menu_id, id_categoria, tipo_contenido )
{
	// Llamado por medio de Ajax
	$.ajax( {
		type : "POST",
		url : "/admin/settings/udgsitemap/ajax",
		data : "menu_id=" + menu_id + "&id_categoria=" + id_categoria + "&tipo_contenido=" + tipo_contenido,
		success : function( datos ) {
			// Asignar valor a los campos
			$( "#edit-categoria-" + menu_id ).html( datos );
		}
	} );
	
	// Cambiar el listado de términos
	cambia_lista_terminos( menu_id, id_categoria );
}





/**
* Función para cambiar las opciones de los selects
* de acuerdo al término
**/
function cambia_lista_terminos( menu_id, id_categoria )
{
	// Llamado por medio de Ajax
	$.ajax( {
		type : "POST",
		async: false,
		url : "/admin/settings/udgsitemap/ajax",
		data : "menu_id=" + menu_id + "&id_categoria=" + id_categoria,
		success : function( datos ) {
			// Asignar valor a los campos
			$( "#edit-termino-" + menu_id ).html( datos );
		}
	} );
	
}





/**
* Función para cambiar las opciones de los selects
* de acuerdo al rol
**/
function cambia_select_rol( menu_id, id_rol )
{
	// Llamado por medio de Ajax
	$.ajax( {
		type : "POST",
		url : "/admin/settings/udgsitemap/ajax",
		data : "menu_id=" + menu_id + "&id_rol=" + id_rol + "&cambia_roles=1",
		success : function( datos ) {			
			// Asignar valor a los campos
			$( "#edit-rol-" + menu_id ).html( datos );
		}
	} );
	
	// Cambiar el listado de usuarios
	cambia_lista_usuarios( menu_id, id_rol, document.getElementById( "#edit-usuario-" + menu_id ) );
}





/**
* Función para cambiar las opciones de los selects
* de acuerdo al usuario
**/
function cambia_lista_usuarios( menu_id, id_rol, id_usuario )
{
	// Llamado por medio de Ajax
	$.ajax( {
		type : "POST",
		async: false,
		url : "/admin/settings/udgsitemap/ajax",
		data : "menu_id=" + menu_id + "&id_rol=" + id_rol + "&id_usuario=" + id_usuario,
		success : function( datos ) {
			// Asignar valor a los campos
			$( "#edit-usuario-" + menu_id ).html( datos );
		}
	} );
	
}





/**
* Función para poner los campos de un item extra
**/
function envia_formulario_extra( prefijo_campos ) 
{	
	document.getElementById( "edit-extras" ).value = $( "#edit-extras-" + prefijo_campos ).val();
	document.getElementById( "edit-extras-titulo" ).value = $( "#edit-extras-titulo-" + prefijo_campos ).val();
	document.getElementById( "edit-extras-liga" ).value = $( "#edit-extras-liga-" + prefijo_campos ).val();
	document.getElementById( "edit-extras-menu" ).value = prefijo_campos;
	
	if(  $( "#edit-extras-editar-id-" + prefijo_campos ).val() != undefined ) {
		document.getElementById( "edit-extras-editar-id" ).value = $( "#edit-extras-editar-id-" + prefijo_campos ).val();
	}
	
}





/**
* Función para editar un item extra
**/
function udgsitemap_editar( id_extra, prefijo_campos )
{
	// Llamada a borrar por medio de AJAX
	$.ajax( {
		  type : "POST",
		  url : "/admin/settings/udgsitemap/ajax",
		  data : "id_editar=" + id_extra,
		  success : function( datos ) {
			  // Separar la respuesta en tokens
			  var arreglo_resp = datos.split( "$" );
			  
			  // Asignar valor a los campos
			  document.getElementById( "edit-extras-" + prefijo_campos ).value = arreglo_resp[0];
			  document.getElementById( "edit-extras-titulo-" + prefijo_campos ).value = arreglo_resp[1];
			  document.getElementById( "edit-extras-liga-" + prefijo_campos ).value = arreglo_resp[2];
			  
			  document.getElementById( "edit-extras-editar-id-" + prefijo_campos ).value = id_extra;
			  
			  // Poner el foco de atención a un elemento del formulario
			  document.getElementById( "edit-extras-" + prefijo_campos ).focus();
		  }
	  } );

}





/**
* Función para borrar un item extra
**/
function udgsitemap_borrar( id_extra )
{
	if( confirmar( "¿Realmente desea borrar el registro?") ){
		
		// Llamada a borrar por medio de AJAX
		$.ajax( {
			type : "POST",
			url : "/admin/settings/udgsitemap/ajax",
			data : "id_borrar=" + id_extra,
			success : function( datos ) {
				// Enviar mensaje de éxito y redireccionar
				alert( "Los cambios fueron realizados con éxito" );
				location.reload();
			}
        } );

	}
}





/**
* Función para confirmar
**/
function confirmar( mensaje_texto )
{
	if( !confirm( mensaje_texto ) ) {
		return false;
	}
	
	return true;
}



