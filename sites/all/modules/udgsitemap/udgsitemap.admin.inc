<?php

// $Id: udgsitemap.admin.inc $

/**
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * UDG Sitemap 6.2
 *
 * Por: Jorge Ramos
 * Julio 2010
 *
 * Desarrollo de la interfaz de admin de UDG Sitemap.
 *
 **/





	/**
	* Funci�n de configuraci�n de las opciones del m�dulo
	* @return array $form: Formulario
	**/
	function udgsitemap_admin() 
	{
		
		// Crear arreglo del formulario
		$form = array();
		
		// Crear variable para tener todos los ids de men�
		$lista_ids_menu = '';
		
		// Crear campos b�sicos de items de men�s extras
		udgsitemap_form_extra( $form );
		
		// Hacer la consulta
		$query = "SELECT DISTINCT( menu_name ), title
				FROM menu_custom
				WHERE menu_name <> 'navigation'
				ORDER BY menu_custom.title
				";
		
		$query_result = db_query( $query );
		
		// Ciclo de la consulta
		while( $links = db_fetch_object( $query_result ) ) 
		{
			
			// Crear fieldsets y campos de formulario para los men�s
			$form[ $links->menu_name ] = array(
				'#type' => 'fieldset',
				'#title' => t( $links->title ),
				'#prefix' => '<div class="mapform_container">',
				'#suffix' => '</div>',
			);
			
			// Arreglo de atributos
			$atr_check = array();
			$opcion_default = '';
			if( udgsitemap_buscar_menu( $links->menu_name ) ) {
				$opcion_default =  $links->menu_name;
			}
			
			// Crear un mostrar para saber si se mostrar� el men�
			$form[ $links->menu_name ][ 'mostrar-' . $links->menu_name ] = array(
				'#type' => 'select',
				'#title' => t( 'Mostrar la informaci&oacute;n de ' . $links->title ),
				'#options' => array( '-1' => 'No', $links->menu_name . '$mostrar' => "Si" ),
				'#default_value' => $opcion_default . '$mostrar'
				);
			
			// Crear fieldset interior para campos extras
			$form[ $links->menu_name ][ $links->menu_name . '_extras' ] = array(
				'#type' => 'fieldset',
				'#title' => t( 'Agregar elementos extras para ' . $links->title ),
				'#collapsible' => TRUE,
  				'#collapsed' => FALSE
			);
			
			// Arreglo de opciones para formulario de men� extras
			$opciones_extras = array();
			
			// Agregar el nombre del men�
			$opciones_extras[ 0 ] = $links->title;
			
			
			
			// Inicio de tabla
			$form[ $links->menu_name ][ 'tabla_inicio_' . $links->menu_name ] = array(
				'#type' => 'item',
				'#value' => '
						<table>
							<thead>
							<tr>
							<th>Item de men&uacute;</th>
							<th>Mostrar</th>
							<th>Tipo de contenido</th>
							<th>Categor&iacute;a</th>
							<th>T&eacute;rmino</th>
							<th>Rol de usuario</th>
							<th>Usuario</th>
							<th>Mostrar submen&uacute;s</th>
							</tr>
							</thead>',
			);
			
			
			
			// Crear el select para ese men�
			udgsitemap_crear_menu_select( $links->menu_name, 0, $form[ $links->menu_name ], $opciones_extras, '', $lista_ids_menu );
			
			
			
			// Traer los elementos de m�s alto nivel para este men�
			$form[ $links->menu_name ][ 'extras_alto_nivel' ] = array(
				'#value' => '<tr class="list_high_level">
								<td colspan="8">
								<ul class="list_edicion">' . udgsitemap_traer_extras_edicion( $links->menu_name, 0 ) . '<ul>
								</td>
							</tr>'
				);
			
			// Fin de tabla
			$form[ $links->menu_name ][ 'tabla_fin_' . $links->menu_name ] = array(
				'#type' => 'item',
				'#value' => '</table>',
			);
			
			
			// Crear formulario de opciones extras
			udgsitemap_form_extra_menu( $links->menu_name, $form[ $links->menu_name ][ $links->menu_name . '_extras' ], $opciones_extras, 0 );
			
			
		} // Fin del ciclo de la consulta
		
		
		
		
		// Crear bot�n de env�o de informaci�n
		$form[ 'boton_guardar' ] = array( 
				'#type' => 'submit',
				'#value' => t( 'Guardar' ),
				'#attributes' => array( 'onclick' => 'envia_formulario()' )
				);
		
		
		return $form;
		
	} // function udgsitemap_admin()
	
	
	
	
	
	/**
	* Trae la informaci�n del men� dentro de campos de selecci�n.
	* @param string $nombre_menu: El nombre del men� para traer los elementos
	* @param integer $padre_menu: Id del elemento padre
	* @param array $form: Arreglo del formulario
	* @param array $opciones_extras: Arreglo de opciones extras para guardar la informaci�n
	* @param string $prefijo: Prefijo del elemento del men� para checar su profundidad
	* @param array $lista_ids_menu: Lista con los id's de los items de men�. Sirve para la inicializaci�n.
	**/
	function udgsitemap_crear_menu_select( $nombre_menu = 'primary-links', $padre_menu = 0, &$form = array(), &$opciones_extras = array(), $prefijo = '', &$lista_ids_menu ) 
	{
		// Crear arreglos auxiliares
		$arreglo_tipos_contenido = array();
		$arreglo_roles = array();
		
		// Traer listado de tipos de contenido, roles.
		$arreglo_tipos_contenido = udgsitemap_regresa_tipos_contenido();
		$arreglo_roles = udgsitemap_regresa_roles();
		
		
		// Hacer la consulta
		$query = "SELECT mlid, link_title, has_children
				FROM menu_links
				WHERE menu_name = '%s'
				AND plid = '%d'
				ORDER BY weight ASC
				";
		$query_result =  db_query( $query, $nombre_menu, $padre_menu );
		
		while( $links = db_fetch_object( $query_result ) ) 
		{
			// Traer listado de tipos de taxonom�as, t�rminos y usuarios
			$arreglo_categorias = array();
			$arreglo_terminos = array();
			$arreglo_usuarios = array();
			
			$arreglo_categorias = udgsitemap_regresa_categorias( udgsitemap_buscar_tipo_cont( $links->mlid ) );
			udgsitemap_regresa_terminos( $arreglo_terminos, udgsitemap_buscar_categoria( $links->mlid ), 0, '', 0 );
			$arreglo_usuarios = udgsitemap_regresa_usuarios( udgsitemap_buscar_rol( $links->mlid ) );
			
			
			// Agregar el id del men� a la variable
			if( udgsitemap_buscar_id_menu( $links->mlid ) != '-1' ) {
				$lista_ids_menu .= $links->mlid . '/' . udgsitemap_buscar_termino( $links->mlid ) . '/' . udgsitemap_buscar_usuario( $links->mlid ) . ',';
			}
			
			// Crear la etiqueta para el elemento
			$form[ 'etq_' . $links->mlid ] = array(
				'#type' => 'item',
				'#title' => $prefijo . ' ' . t( $links->link_title ),
				'#prefix' => '<tr><td>',
				'#suffix' => ''
				);
			
			// Crear el campo oculto para el valor final
			$form[ 'valor_' . $links->mlid ]= array(
				'#type' => 'select',
				'#type' => 'hidden',
				'#value' => '-1',
				'#prefix' => '',
				'#suffix' => '</td>'
				);
			
			// Crear el campo para mostrar o no el men�
			$form[ 'mid_' . $links->mlid ] = array(
				'#type' => 'select',
				'#options' => array( 
					'-1' => 'No',
					$links->mlid => 'Si'
					),
				'#value' => udgsitemap_buscar_id_menu( $links->mlid ),
				'#prefix' => '<td align="center"><div class="etq_form_item menu_id">',
				'#suffix' => '</div></td>'
				);
			
			// Crear el campo de tipo de contenido
			$form[ 'tipo_contenido_' . $links->mlid ] = array(
				'#type' => 'select',
				'#options' => $arreglo_tipos_contenido,
				'#value' => udgsitemap_buscar_tipo_cont( $links->mlid ),
				'#attributes' => array( 'onchange' => 'cambia_selects_tipo_contenido( "' . $links->mlid . '", this.value, document.getElementById( "edit-categoria-' . $links->mlid . '" ).value )' ),
				'#prefix' => '<td align="center">',
				'#suffix' => '</td>'
				);
			
			// Crear el campo de categor�as (taxonom�as)
			$form[ 'categoria_' . $links->mlid ] = array(
				'#type' => 'select',
				'#options' => $arreglo_categorias,
				'#value' => udgsitemap_buscar_categoria( $links->mlid ),
				'#attributes' => array( 'onchange' => 'cambia_select_categoria( "' . $links->mlid . '", this.value, document.getElementById( "edit-tipo-contenido-' . $links->mlid . '" ).value )' ),
				'#prefix' => '<td align="center">',
				'#suffix' => '</td>'
				);
			
			// Crear el campo de t�rminos
			$form[ 'termino_' . $links->mlid ] = array(
				'#type' => 'select',
				'#options' => $arreglo_terminos,
				'#value' => udgsitemap_buscar_termino( $links->mlid ),
				'#prefix' => '<td align="center">',
				'#suffix' => '</td>'
				);
			
			// Crear el campo de roles
			$form[ 'rol_' . $links->mlid ] = array(
				'#type' => 'select',
				'#options' => $arreglo_roles,
				'#value' => udgsitemap_buscar_rol( $links->mlid ),
				'#attributes' => array( 'onchange' => 'cambia_select_rol( "' . $links->mlid . '", this.value )' ),
				'#prefix' => '<td align="center">',
				'#suffix' => '</td>'
				);
			
			// Crear el campo de usuarios
			$form[ 'usuario_' . $links->mlid ] = array(
				'#type' => 'select',
				'#options' => $arreglo_usuarios,
				'#value' => udgsitemap_buscar_usuario( $links->mlid ),
				'#prefix' => '<td align="center">',
				'#suffix' => '</td>'
				);
			
			// Crear el campo de tiene hijos
			$form[ 'tiene_hijos_' . $links->mlid ] = array(
				'#type' => 'select',
				'#options' => array( 
					'0' => 'No',
					'1' => 'Si'
					),
				'#value' => udgsitemap_buscar_tiene_hijos( $links->mlid ),
				'#prefix' => '<td align="center">',
				'#suffix' => '</td></tr>'
				);
			
			
			
			// Traer los elementos de m�s alto nivel para este men�
			$form[ 'extras_alto_nivel' . $links->mlid ] = array(
				'#value' => '<div class="list_high_level"><ul class="list_edicion">' . udgsitemap_traer_extras_edicion( $links->mlid ) . '</ul></div>',
				'#prefix' => '<tr style="border-bottom: 1px dotted #CCCCCC; margin-bottom: 10px;"><td colspan="8">',
				'#suffix' => '</td></tr>'
				);
			
						
			// Agregar al menu de opciones extras
			$opciones_extras[ $links->mlid ] = $prefijo . $links->link_title;
			
			// Si tiene hijos, volver a llamar la funci�n
			if( $links->has_children == '1' ){
				udgsitemap_crear_menu_select( $nombre_menu, $links->mlid, $form, $opciones_extras, $prefijo . '--', $lista_ids_menu );
			}
			
			
			
		}
		
		
	} // function udgsitemap_crear_menu_select()
	
	
	
	
	
	/**
	* Crea un formulario para los campos ocultos para los
	* items extras de men�.
	* @param array $form: Arreglo del formulario
	**/
	function udgsitemap_form_extra( &$form = array() ) 
	{
		
		// Crear el campo de nombre de men�
		$form[ 'extras_menu' ] = array(
			'#type' => 'hidden',
			'#value' => '-1'
			);
		
		// Crear el campo de id de men�
		$form[ 'extras' ] = array(
			'#type' => 'hidden',
			'#value' => '-1'
			);
		
		// Crear el campo de t�tulo
		$form[ 'extras_titulo' ] = array(
			'#type' => 'hidden',
			'#value' => '-1'
			);
		
		// Crear el campo de t�tulo
		$form[ 'extras_liga' ] = array(
			'#type' => 'hidden',
			'#value' => '-1'
			);
		
		// campo para saber si hay que editar
		$form[ 'extras_editar_id' ] = array(
				'#type' => 'hidden',
				'#value' => '-1'
			);
		
	}
	
	
	
	
	
	/**
	* Crea un formulario para las opciones de items extras del men�
	* @param string $nombre_menu: El nombre del men� al que pertenece
	* @param array $form: Arreglo del formulario
	* @param array $opciones_extras: Arreglo de las opciones extras
	* @param integer $padre_menu: Id del elemento padre
	**/
	function udgsitemap_form_extra_menu( $nombre_menu = 'primary-links', &$form = array(), &$opciones_extras = array(), $padre_menu = 0 ) 
	{
				
		// Crear elemento de formulario para saber si va a haber edici�n
		$form[ 'extras_editar_id_' . $nombre_menu ] = array(
			'#type' => 'hidden',
			'#default_value' => '-1'
		);
		
		// Crear el listado de las opciones
		$form[ 'extras_' . $nombre_menu ] = array(
			'#type' => 'select',
			'#title' => t( 'Elemento extra para ' . $opciones_extras[ $nombre_menu ] ),
			'#options' => $opciones_extras,
			'#default_value' => ''
			);
		
		// Crear el campo de t�tulo
		$form[ 'extras_titulo_' . $nombre_menu ] = array(
			'#type' => 'textfield',
			'#title' => t( 'T&iacute;tulo' ),
			'#maxlength' => 100,
          	'#size' => 50,
			'#default_value' => ''
			);
		
		// Crear el campo de t�tulo
		$form[ 'extras_liga_' . $nombre_menu ] = array(
			'#type' => 'textfield',
			'#title' => t( 'Enlace' ),
			'#maxlength' => 100,
          	'#size' => 50,
			'#default_value' => ''
			);
		
		// Crear bot�n de env�o de informaci�n
		$form[ 'extras_boton' ] = array( 
			'#type' => 'submit',
			'#value' => t( 'Guardar item extra' ),
			'#attributes' => array( 'onclick' => 'envia_formulario_extra( "' . $nombre_menu . '" )' )
			);
		
	} // function udgsitemap_form_extra_menu()
	
	
	
	
	
	/**
	* Validaci�n del formulario de configuraci�n
	* @param array $form: Arreglo asociativo que contiene la estructira del formulario
	* @param array $form_state: Arreglo indexado que contiene el estado actual del formulario
	**/
	function udgsitemap_admin_validate( $form, &$form_state ) {
		
	} // function udgsitemap_admin_validate()
	
	
	
	
	
	/**
	* Tareas a realizar luego del submit, para el guardado de la informaci�n
	* @param array $form: Arreglo asociativo que contiene la estructira del formulario
	* @param array $form_state: Arreglo indexado que contiene el estado actual del formulario
	**/
	function udgsitemap_admin_submit( $form, &$form_state ) 
	{		
		// Guardar los items extras si es que existen sus valores
		if( $form[ '#post' ][ 'extras' ] != '-1' && $form[ '#post' ][ 'extras_titulo' ] != '-1' && 
			$form[ '#post' ][ 'extras_liga' ] != '-1' && $form[ '#post' ][ 'extras_menu' ] != '-1' ) {
			
			// Guardar resultados en la tabla de items extras
			if( $form[ '#post' ][ 'extras_editar_id' ] == '-1' ) {
				db_query( "INSERT INTO udgsitemap_extras VALUES ( '', '%s', '%d', '%s', '%s' )",
							$form[ '#post' ][ 'extras_menu' ],
							$form[ '#post' ][ 'extras' ],
							$form[ '#post' ][ 'extras_titulo' ],
							$form[ '#post' ][ 'extras_liga' ]
				);
			}
			
			// Editar
			else{
				db_query( "UPDATE udgsitemap_extras 
						 	SET menu_name = '%s',
							id_menu = '%d',
							titulo = '%s',
							liga = '%s'
							WHERE id = '%d' ",
							$form[ '#post' ][ 'extras_menu' ],
							$form[ '#post' ][ 'extras' ],
							$form[ '#post' ][ 'extras_titulo' ],
							$form[ '#post' ][ 'extras_liga' ],
							$form[ '#post' ][ 'extras_editar_id' ]
				);
			}
			
		}
		
		
		// Si lo que hay que guardar es el resto del formulario
		else
		{
			// Borrar primero el contenido de la base de datos
			db_query( "DELETE FROM udgsitemap" );
			db_query( "DELETE FROM udgsitemap_menu" );
			
			// Arreglo auxiliar para los tokens separados
			$tokensseparados = array();
			
			// Meter el contenido del arreglo en pares id_menu / tipo_contenido
			foreach( $form[ '#post' ] as $resaux ){
				
				// Verificar que el tipo de contenido no est� vac�o y guardar el listado de item de men�
				if( $resaux != "-1" && strpos( $resaux, "/" ) != false )
				{
					// Separar los tokens
					$tokensseparados = udgsitemap_separatokens( $resaux );
					db_query( "INSERT INTO udgsitemap VALUES ('%d', '%s', '%d', '%d', '%d', '%d', '%d' )",
							$tokensseparados[ 0 ], $tokensseparados[ 1 ], $tokensseparados[ 2 ], 
							$tokensseparados[ 3 ], $tokensseparados[ 4 ], $tokensseparados[ 5 ],
							$tokensseparados[ 6 ] );
				}
				
				// Si el checkbox de mostrar est� activado
				else if( $resaux != "-1" && strpos( $resaux, "$" ) != false ) {
					
					// Obtener el valor a guardar
					$arreglo_mostrar = explode( "$", $resaux );
					
					// Guardar en la tabla de men�s a imprimirse
					db_query( "INSERT INTO udgsitemap_menu VALUES ( '%s' )", $arreglo_mostrar[ 0 ] );
					
				}
			
			}
		}
	
		// Mandar mensaje de �xito
		drupal_set_message( t( 'Las opciones han sido guardadas' ) );
	
	} // function udgsitemap_admin_submit()
	
	
	
	
	
	/**
	* Devuelve arreglo con los tipos de contenido
	* @return array $arreglo_resultado: Arreglo con la lista de los tipos de contenido
	**/
	function udgsitemap_regresa_tipos_contenido() 
	{
		
		$arreglo_resultado = array();
		
		// Inicializar arreglo
		$arreglo_resultado[ '0' ] = "Ninguno";
		
		// Hacer la consulta
		$query = "SELECT type, name
				FROM node_type
				ORDER BY type
				";
		
		$query_result =  db_query( $query );
		while( $elemento = db_fetch_object( $query_result ) ) 
		{
			// Agregar elemento al arreglo
			$arreglo_resultado[ $elemento->type ] = $elemento->name;
		}
		
		// Regresar el resultado
		return $arreglo_resultado;
		
	} // function udgsitemap_regresa_tipos_contenido() 
	
	
	
	
	
	/**
	* Devuelve arreglo con las taxonom�as (categor�as)
	* @return array $arreglo_resultado: Arreglo con la lista de las categor�as o taxonom�as
	**/
	function udgsitemap_regresa_categorias( $tipo_contenido ) 
	{
		
		$arreglo_resultado = array();
		
		// Inicializar arreglo
		$arreglo_resultado[ '-1' ] = "Ninguna";
		$arreglo_resultado[ '0' ] = "Todas";
		
		// Hacer la consulta
		$query = "SELECT DISTINCT( v.vid ) AS vid, v.name AS name
					FROM vocabulary AS v, vocabulary_node_types AS vn
					WHERE v.vid = vn.vid
					AND vn.type = '%s' ";
		
		$query_result =  db_query( $query, $tipo_contenido );
		while( $elemento = db_fetch_object( $query_result ) ) 
		{
			// Agregar elemento al arreglo
			$arreglo_resultado[ $elemento->vid ] = $elemento->name;
		} 
		
		// Regresar el resultado
		return $arreglo_resultado;
		
	} // function udgsitemap_regresa_categorias()
	
	
	
	
	
	/**
	* Devuelve arreglo con los t�rminos
	* @param array $arreglo_resultado: Arreglo con la lista de los t�rminos
	* @param integer $termino_padre: Id del t�rmino padre
	* @param string $prefijo: Prefijo para mostrar la profundidad del elemento
	* @param integer $contador_prof: Contador de la profundidad, para cortar el ciclo recursivo
	**/
	function udgsitemap_regresa_terminos( &$arreglo_resultado, $id_vocabulario, $termino_padre, $prefijo, $contador_prof = 0 ) 
	{
				
		// Inicializar arreglo
		if( $contador_prof == 0 ) {
			$arreglo_resultado[ '-1' ] = "Ninguno";
			$arreglo_resultado[ '0' ] = "Todos";
		}
		
		// Si el contador de profundidad es mayor a 2, regresar valor vac�o
		else if( $contador_prof > 2 ) {
			return;
		}
		
		
		
		// Hacer la consulta
		$query = "SELECT DISTINCT( td.tid ), td.name
				FROM term_data AS td, term_hierarchy AS th
				WHERE td.vid = '%d'
				AND td.tid = th.tid
				AND th.parent = '%d'
				ORDER BY td.weight";
		
		$query_result =  db_query( $query, $id_vocabulario, $termino_padre );
		while( $elemento = db_fetch_object( $query_result ) ) 
		{
			// Agregar elemento al arreglo
			$arreglo_resultado[ $elemento->tid ] = $prefijo . ' ' . $elemento->name;
			
			
			// Hacer una consulta en la tabla de jerarqu�as para saber si ese t�rmino tiene hijos
			$query_hijos = "SELECT COUNT( DISTINCT tid ) AS total_hijos FROM term_hierarchy AS th
				WHERE th.parent = '%d' ";
			
			$query_hijos_res = db_query( $query_hijos, $elemento->tid );
			if( $elemento_hijos = db_fetch_object( $query_hijos_res ) ){
				
				// Si hay hijos, entonces hacer llamada recursiva a la funci�n
				if( $elemento_hijos->total_hijos ){
					udgsitemap_regresa_terminos( $arreglo_resultado, $id_vocabulario, $elemento->tid, $prefijo . '--', ( $contador_prof + 1 ) );
				}
				
			}
		}
		
		// Regresar el resultado
		return $arreglo_resultado;
		
	} // function udgsitemap_regresa_terminos()
	
	
	
	
	
	/**
	* Devuelve arreglo con los roles
	* @return array $arreglo_resultado: Arreglo con la lista de los roles
	**/
	function udgsitemap_regresa_roles() 
	{
		
		$arreglo_resultado = array();
		
		// Inicializar arreglo
		$arreglo_resultado[ '-1' ] = "Ninguno";
		$arreglo_resultado[ '0' ] = "Todos";
		
		// Hacer la consulta
		$query = "SELECT r.rid, r.name FROM role AS r
				WHERE r.rid <> '1'
				AND r.rid <> '2'
				ORDER BY r.rid";
		
		$query_result =  db_query( $query );
		while( $elemento = db_fetch_object( $query_result ) ) 
		{
			// Agregar elemento al arreglo
			$arreglo_resultado[ $elemento->rid ] = $elemento->name;
		}
		
		// Regresar el resultado
		return $arreglo_resultado;
		
	} // function udgsitemap_regresa_roles()
	
	
	
	
	
	/**
	* Devuelve arreglo con los usuarios	
	* @return array $arreglo_resultado: Arreglo con la lista de los usuarios
	**/
	function udgsitemap_regresa_usuarios( $id_rol ) 
	{
		
		$arreglo_resultado = array();
		
		// Inicializar arreglo
		$arreglo_resultado[ '-1' ] = "Ninguno";
		$arreglo_resultado[ '0' ] = "Todos";
		
		// Hacer la consulta
		$query = "SELECT u.uid, u.name
				FROM users AS u, users_roles AS ur
				WHERE u.uid = ur.uid
				AND u.name <> '' 
				AND ur.rid = '%d' ";
		
		$query_result =  db_query( $query, $id_rol );
		while( $elemento = db_fetch_object( $query_result ) ) 
		{
			// Agregar elemento al arreglo
			$arreglo_resultado[ $elemento->uid ] = $elemento->name;
		}
		
		// Regresar el resultado
		return $arreglo_resultado;
		
	} // function udgsitemap_regresa_usuarios() 
	
	
	
	
	
	/**
	* Traer la informaci�n de los items de men� extras con enlaces para edici�n
	* @param integer $id_menu: Id del elemento de men�
	* @return string $lista_extras: Cadena con el contenido
	**/
	function udgsitemap_traer_extras_edicion( $id_menu ) 
	{
		
		// Variable a retornar
		$lista_extras = '';
		
		// Hacer la consulta
		$query = "SELECT * FROM udgsitemap_extras
			WHERE id_menu = '%d'
			ORDER BY id";
		
		$query_result =  db_query( $query, $id_menu );
		while( $res_extras = db_fetch_object( $query_result ) ) 
		{
			
			// Item de la lista con link
			$lista_extras .= '<li class="item_edicion">
								<div class="original">' . l( $res_extras->titulo, $res_extras->liga ) . '</div>
								<div class="btn_edicion"> &raquo; <a href="#a" onclick="udgsitemap_editar( ' . $res_extras->id . ', \'' . $res_extras->menu_name . '\' )">Editar</a></div>
								<div class="btn_edicion"> &raquo; <a href="#a" onclick="udgsitemap_borrar( ' . $res_extras->id . ' )">Borrar</a></div>
							</li>';
			
		}
		
		// Regresar resultado
		return $lista_extras;
		
	} // function udgsitemap_traer_extras_edicion()
	
	
	
	