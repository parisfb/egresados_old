<?php
/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * TEMPLATE DE BLOQUE
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 *
 * Todos los bloques del tema se contruyen en base a este archivo.
 *
 */

/**
 * @file block.tpl.php
 *
 * Implementaci�n del tema para mostrar un bloque.
 *
 * Variables disponibles:
 * - $block->subject: T�tulo del bloque.
 * - $block->content: Contenido del bloque.
 * - $block->module: M�dulo que genera el bloque.
 * - $block->delta: Es un id num�rico conectado a cada m�dulo.
 * - $block->region: La regi�n donde est� inclu�do el bloque actual.
 * - $classes: El paquete de clases de CSS para el DIV que envuelve al bloque.
     Los posibles valores son: block-MODULE, region-odd, region-even, odd, even,
     region-count-X, and count-X.
 *
 * Variables de ayuda:
 * - $block_zebra: Imprime 'odd' y 'even' dependiendo de cada regi�n.
 * - $zebra: La misma salida que $block_zebra pero independiente de la regi�n.
 * - $block_id: Contador dependiente de cada regi�n.
 * - $id: La misma salida que $block_id pero independiente de la regi�n.
 * - $is_front: Variable bandera que es verdadera (true) cuando est� presente en la p�gina principal.
 * - $logged_in: Variable bandera que es verdadera (true) cuando el usuario actual es un usuario logeado.
 * - $is_admin: Variable bandera que es verdadera (true) cuando el usuario actual es un administrador.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 */
?>

<div id="block-udg-<?php print $block->module . '-' . $block->delta; ?>" class="<?php print $classes; ?>">
<?php if ($block->subject){  ?>
 
    <div class="block-inner cBorded">
    <div class="block_ctl">
      <div class="block_ctr">
      <h2 class="block_title"> <span class="block_ctlH"> <span class="block_ctrH"> <?php print $block->subject; ?> </span> </span> </h2>
         <div class="block_cbr">
          <div class="block_cbl">
          
            <div class="content"> <?php print $block->content; ?>
              <div class="clear-both"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    
    
  <?php } else{ ?>
  <div class="block-inner">
   
            <div class="content"> <?php print $block->content; ?>
              <div class="clear-both"> </div>
      
    </div>
    <?php } ?>
    <?php print $edit_links; ?> </div>
</div>
<!-- /block-inner, /block -->
