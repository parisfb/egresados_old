/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * ARCHIVO DE FUNCIONES PARA SLIDESHOW
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 *
 * Todos los slideshows y slideshowtexts obtienen sus efectos y la programaci�n javascript de este archivo.
 *
 */


// Funci�n para agregar clases y crear el efecto del slide
// Par�metros:
// - id: Cadena con el identificador del bloque de slideshow.
// - classes: Clase del slideshow para definir con CSS el tama�o y tipo del bloque.
//		Posibles valores:
//		- 1: Slideshow con ancho de bloque de 970px y ancho de imagen de 960px;
//		- 2: Slideshowtext con ancho de bloque de 970px y ancho de imagen de 715px;
//		- 3: Slideshow con ancho de bloque de 720px y ancho de imagen de 710px;
//		- 4: Slideshowtext con ancho de bloque de 720px y ancho de imagen de 475px;
//		- 5: Slideshow con ancho de bloque de 480px y ancho de imagen de 470px;
//		- 6: Slideshowtext con ancho de bloque de 480px y ancho de imagen de 235px;
//		- 7: Slideshow con ancho de bloque de 230px y ancho de imagen de 220px;
// - ini: Variable para saber si el efecto de slide va a ejecutarse o no. �til para hacer depuraciones.
var UDGswitchSlide	= function( id, classe, ini )
{
	if( ini)
	{
		$("#" + id).addClass("udg_slideshowtext_block" + classe);
		$("#" + id).addClass("udg_slideshowtext_block_global");
		$('#' + id + ' .block-inner .content .view .view-content').each(function() {
			var slideshow = $(this);
			var number = 0;
			var pager = $('<div class="slideshow-pager slideshow-pager'  + classe +  '">')
			$(this).children('div').each(function() {
				var slide = $(this);
				number++;
				var link = $('<a>'+ number +'</a>');
				pager.append(link);
				link.click(function() {
					//slideshow_play = true;
					slide.hide();
					slideshow.append(slide);
					pager.children('.active').removeClass('active');
					$(this).addClass('active');
					slide.fadeIn('slow');
				});
			});
			slideshow.after(pager);
		});
		setInterval( 'UDGswitchSlide("' + id + '","' + classe + '",0)', 6000);
	}
	
	var slideshow	= $('#' + id + ' .block-inner .content .view .view-content');
	var slide		= $('#' + id + ' .block-inner .content .view .view-content > div:first');
	var pager		= $('#' + id + ' .block-inner .content .view .slideshow-pager');

	slide.hide();
	slideshow.append(slide);
	pager.children('.active').removeClass('active').next('a').addClass('active');
	if (!pager.children('.active').length) pager.children('a:first').addClass('active');
	slide.fadeIn('slow');	
};


// Esta funci�n se ejecuta al terminar de cargarse la p�gina.
// Va llamando a la funci�n UDGswitchSlide por cada uno de los bloques creados para tener efecto slide.
// El �ltimo par�metro (ini) se define en 1 por default para agregar el efecto y los estilos.
$(document).ready(
function() 
{
	UDGswitchSlide("block-udg-views-vista_slideshow-block_1"        ,1, true );
	UDGswitchSlide("block-udg-views-vista_slideshowtext-block_1"    ,2, true );
	UDGswitchSlide("block-udg-views-vista_udg_slideshow-block_1"    ,3, true );
	UDGswitchSlide("block-udg-views-vista_udg_slideshow2-block_1"   ,4, true );
	UDGswitchSlide("block-udg-views-vista_udg_slideshow3-block_1"   ,5, true );
	UDGswitchSlide("block-udg-views-vista_udg_slideshow4-block_1"   ,6, true );
	UDGswitchSlide("block-udg-views-vista_udg_slideshow5-block_1"   ,7, true );
});
