<?php
/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * TEMPLATE DE ELEMENTOS (ITEMS) DE MENU
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 *
 * Todos los elementos (items) de un men� se construyen a partir de este archivo.
 *
*/

/* Este template es llamado desde la funci�n phptemplate_menu_links() en el template.php de DrUDG.
 * Variables disponibles:
       $link_is_active - Se puede usar para asignar la clase 'active' a los elementos activos de men�. La funci�n The override REMUEVE
	   		la asignaci�n autom�tica, as� que, si as� lo desea, debe hacer esto en template.php;
       $link_to'  - En enlace del elemento.
       $classes - Clases asignadas por Drupal.
       $position' - Puede ser usado como un nombre de clase. Posibles valores: 'first', 'middle', 'last'.
      'item_id' - Identificador �nico del elemento del men� y luce como 'menu-1-2-3' � 'menu-1-2-3-active';
      'attributes' - Otros atributos HTML del enlace, como el ID, ya formateado por Drupal.
      'title' - El texto mostrado para el enlace.      
*/
?>

  <li id="<?php print $item_id; ?>" <?php if ($link_is_active): ?> class="active"  <?php endif; ?> >
    <?php print generar_menu_zen($link, $has_children, $menu, "" , "",$template_files); ?>
  </li>