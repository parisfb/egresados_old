<?php
/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * TEMPLATE DE P�GINA
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 *
 * Todas las p�ginas del tema se contruyen en base a este archivo.
 *
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?>
<link media="all" rel="stylesheet" type="text/css" href="http://www.udg.mx/menu/udg-menu.css" />
<?php print $styles; ?><?php print $scripts; ?>
<!-- Carga de la barra de accesos de la UdeG -->
<script type="text/javascript" src="http://www.udg.mx/menu/udg-menu.js"></script>
<script type="text/javascript">
	var UDGMenuWidth ='970px';
	document.writeln( crearTag( 'div', 'id="udg_menu_principal"' ) ); setTimeout( 'UDGMenu()', 2000 );
</script>
<link rel='stylesheet' type='text/css' href='http://www.udg.mx/menu/udg-menu.css'>
<!-- Fin de la carga de la barra de accesos de la UdeG -->
</head>

<body class="<?php print $body_classes; ?>">
<div id="page">
  <div id="page-inner">
    <div id="header">
      <div id="header-inner" class="clear-block">
        <div id="sign">
          <div id="bloque_logo" > <a href="http://udg.mx" title="Universidad de Guadalajara" > <img src="/themes/drudg/images/logo.jpg" alt="Universidad de Guadalajara" /> </a> </div>
          <div id="wrap_sign_aux">
            <div id="aux_navigation">
              <?php if ( $search_box ) : ?>
              <?php endif; ?>
              <ul class="links" id="menu_login">
                <?php
								if( $GLOBALS['user'] -> uid != '' )
								{
								?>
                <li class="menu-658 first cerrar_sesion"> Bienvenido <a href="<?php print $base_path; ?>user"><?php print $GLOBALS["user"] -> name; ?></a>, <a title="" href="<?php print $base_path; ?>?q=logout">Cerrar Sesi&oacute;n</a> </li>
                <?php
								}
								else
								{
								?>
                <li class="menu-658 first cerrar_sesion"> <a title="" href="<?php print $base_path; ?>?q=user" >Iniciar Sesi&oacute;n</a> </li>
                <?php 
								}
								?>
              </ul>
              <?php print theme( 'links', $secondary_links ); ?>
              <ul id="aux_navigation_fecha" class="links" >
                <li id="sign_date"> <?php print format_date( time(), 'custom', t( 'l j \de F \de Y' ) );  ?> </li>
              </ul>
            </div>
            <!-- Termina aux_navigation-->
            <div id="search-box"> <?php print $search_box; ?> </div>
            <!-- Termina search-box -->
          </div>
          <!-- Termina wrap_sign_aux-->
        </div>
        <!-- Termina sign-->
        <?php if ( $logo or $site_name or $site_slogan ): ?>
        <div id="wrap_title">
          <?php if ( $site_name ) : ?>
          <div id="title">
            <h1> <a href="<?php print $base_path; ?>" title="<?php print t( 'Home' ); ?>" rel="home"> <?php print $site_name; ?></a> </h1>
          </div>
          <!-- Termina title-->
          <?php endif; ?>
          <?php if ( $logo ) : ?>
          <div id="aux_title"> <a href="<?php print $base_path; ?>" title="<?php print t( 'Home' ); ?>" rel="home"> <img src="<?php print $logo; ?>" alt="<?php print t( 'Home' ); ?>" id="logo-image" /></a> </div>
          <!-- Termina Aux-->
          <?php endif; ?>
        </div>
        <!-- Termina Wrap_title-->
        <?php endif; ?>
        <?php if ( $primary_links or $secondary_links or $navbar ) : ?>
        <div id="navbar">
          <div id="navbar-inner" class="region region-navbar"> <a name="navigation" id="navigation"></a>
            <?php if ( $primary_links ) : ?>
            <div id="primary">
              <?php 
								print theme( 'nice_menu_primary_links' );
							 ?>
            </div>
            <!-- Termina primary -->
            <?php endif; ?>
            <?php print $navbar; ?> </div>
        </div>
        <!-- Termina navbar-inner, Termina navbar -->
        <?php endif; ?>
        <?php if ( $alert or $messages  ) : ?>
        <div id="alert"> <?php print $alert; ?>  <?php print $messages; ?> </div>
        <!-- Termina alert-->
        <?php endif; ?>
        <?php if ( $header ): ?>
        <div id="header-blocks" class="region region-header"> <?php print $header; ?> </div>
        <!-- Termina header-blocks -->
        <?php endif; ?>
      </div>
    </div>
    <!-- Termina header-inner, Termina header -->
    <?php if ( $before_content ) : ?>
    <div id="before_content"> <?php print $before_content; ?> </div>
    <!-- Termina Before Content-->
    <?php endif; ?>
    <div id="main">
      <div id="main-inner" class="<?php if ( $search_box or $primary_links or $secondary_links or $navbar ) {  } ?>">
        <?php if( $left ) : ?>
        <div id="sidebar-left">
          <div id="sidebar-left-inner" class="region region-left"> <?php print $left; ?> </div>
        </div>
        <!-- Termina sidebar-left-inner, Termina sidebar-left -->
        <?php endif; ?>
        <div id="content">
          <div id="content-inner">
            <?php if ( $mission ) : ?>
            <div id="mission"><?php print $mission; ?></div>
            <?php endif; ?>
            <?php if ($content_top): ?>
            <div id="content-top" class="region region-content_top"> <?php print $content_top; ?> </div>
            <!-- Termina content-top -->
            <?php endif; ?>
            <?php if ( $breadcrumb or $title or $tabs or $help ) : ?>
            <div id="content-header"> <?php print $breadcrumb; ?>
              <?php if ( $title ): ?>
              <h1 class="title"><?php print $title; ?></h1>
              <?php endif; ?>
              <?php if ($tabs): ?>
              <div class="tabs"><?php print $tabs; ?></div>
              <?php endif; ?>
              <?php print $help; ?> </div>
            <!-- Termina content-header -->
            <?php endif; ?>
            <div id="content-area">
              <?php
				// Incluir funci�n de protecci�n de correos
				proteger_correo( $content );
			  ?>
              <?php print $content; ?> </div>
            <?php if( $feed_icons ) : ?>
            <div class="feed-icons"><?php print $feed_icons; ?></div>
            <?php endif; ?>
            <?php if( $content_bottom ) : ?>
            <div id="content-bottom" class="region region-content_bottom"> <?php print $content_bottom; ?> </div>
            <!-- Termina content-bottom -->
            <?php endif; ?>
          </div>
        </div>
        <!-- Termina content-inner, Termina content -->
        <?php if( $right ) : ?>
        <div id="sidebar-right">
          <div id="sidebar-right-inner" class="region region-right"> <?php print $right; ?> </div>
        </div>
        <!-- Termina sidebar-right-inner, Termina sidebar-right -->
        <?php endif; ?>
      </div>
      <div class="clear-both"></div>
    </div>
    <!-- Termina main-inner, Termina main -->
    <?php if( $after_content ) : ?>
    <div id="after_content"> <?php print $after_content; ?> </div>
    <!-- Termina After Content-->
    <?php endif; ?>
  </div>
</div>
<!-- Termina page-inner, Termina page -->
<?php if( $footer or $footer_message ) : ?>
<div id="footer">
  <div id="footer-inner" class="region region-footer">
    <div id="footer_menu"> <?php print theme( 'links', $primary_links ); ?> <?php print theme( 'links', $secondary_links ); ?> </div>
    <div id="footer_firm"> <?php print $footer; ?> </div>
    <?php if( $footer_message ) : ?>
    <div id="footer-message"><?php print $footer_message; ?></div>
    <?php endif; ?>
  </div>
</div>
<!-- Termina footer-inner, Termina footer -->
<?php endif; ?>
<?php if( $closure_region ) : ?>
<div id="closure-blocks" class="region region-closure"><?php print $closure_region; ?></div>
<?php endif; ?>
<?php print $closure; ?>
</body>
</html>
