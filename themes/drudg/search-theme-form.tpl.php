<?php
/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * TEMPLATE DE FORMULARIO DE B�SQUEDA
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 *
 */

/**
 * @file search-theme-form.tpl.php
 * Implementaci�n por default del tema DrUDG para mostrar un formulario de b�squeda directamente dentro del
 * template del tema. 
 * IMPORTANTE: No confundir con el bloque de b�squeda o con la p�gina de b�squeda, propios de Drupal.
 *
 * Variables disponibles:
 * - $search_form: El formulario de b�squeda completo, listo para ser impreso.
 * - $search: Arreglo de elementos de b�squeda indexados. Puede ser usado para imprimir cada elemento 
 *   del formulario de manera separda.
 *
 * Llaves por default en el arreglo $search:
 * - $search['search_theme_form']: �rea del campo de texto envuelta en un div.
 * - $search['submit']: Bot�n de env�o de formulario.
 * - $search['hidden']: Elementos de formulario escondidos (hidden). Utilizados para validar el env�o de formularios
 *
 * Ya que el arreglo $search est� indexado, es posible hacer una impresi�n directa.
 * Los m�dulos pueden agregarse al formulario de b�squeda por lo que es recomendable verificar
 * su existencia antes de imprimir. Las llaves por default simpre existen.
 * 
 * C�digo de ejemplo:
 *   <?php if (isset($search['extra_field'])): ?>
 *     <div class="extra-field">
 *       <?php print $search['extra_field']; ?>
 *     </div>
 *   <?php endif; ?>
 *
 * Para verificar toda la informaci�n disponible en el arreglo $search, s�rvase de utilizar el c�digo siguiente:
 *
 *   <?php print '<pre>'. check_plain(print_r($search, 1)) .'</pre>'; ?>
 *
 * @see template_preprocess_search_theme_form()
 */
?>

<div id="search" class="container-inline">
  <?php print $search_form; ?>
</div>
