/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * FUNCIONES DE LOGIN EN LA RED UNIVERSITARIA
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 *
 * Este archivo proporciona la interfaz para iniciar sesiones en los sistemas de correo, SIIAU y otros
 * dentro de la Red Universitaria.
 *
 */


// Funci�n para el cambio de pesta�a en el bloque, para pasar de Correo a SIIAU
var logintabcorreo	= function()
{
	$('#login-panel-forma-siiau').hide();
	$('#login-panel-forma-correo').show();
	$('#login-panel-correo').removeClass('login-panel-no-seleccionado');
	$('#login-panel-siiau').addClass('login-panel-no-seleccionado');
};

// Funci�n para el cambio de pesta�a en el bloque, para pasar de SIIAU a Correo
var logintabsiiau	= function()
{
	$('#login-panel-forma-siiau').show();
	$('#login-panel-forma-correo').hide();
	$('#login-panel-correo').addClass('login-panel-no-seleccionado');
	$('#login-panel-siiau').removeClass('login-panel-no-seleccionado');
};

// Funci�n que sirve para determinar a qu� instancia de Correo UDG se intenta accesar
// En base a esto, hace la redirecci�n al servidor correspondiente
var handleEmail	= function(){
var email = document.getElementById('email-field').value;

	var splitIndex = email.lastIndexOf("@");

	if(splitIndex != -1){
		username = email.substring(0, splitIndex);
		domainName = email.substring(splitIndex + 1);
	}
	var mailServer		= '';
	var destination		= '';
	var value_password	= document.getElementById('password').value;
	var value_username	= username;
	
	document.getElementById('email-field').value	= username;
	document.getElementById('username').value		= username;
	document.getElementById('password').value		= document.getElementById('password-field').value;
	
	var https		= false;
	var validDomain	= false;
	switch(domainName){
		case "cencar.udg.mx":
			mailServer = 'webmail.udg.mx:80';
			validDomain = true;
			break;
		case "mail.udg.mx":
			mailServer = 'webmail.udg.mx:80';
			validDomain = true;
			break;
		case "cualtos.udg.mx":
			mailServer = 'webmail.udg.mx:80';
			validDomain = true;
			break;
		case "cucba.udg.mx":
			mailServer = 'maiz.cucba.udg.mx:81';
			validDomain = true;
			break;
		case "lagos.udg.mx":
			mailServer = 'afrodita.lagos.udg.mx:80';
			validDomain = true;
			break;
		case "red.cucei.udg.mx":
			mailServer = 'quantum.red.cucei.udg.mx:80';
			validDomain = true;
			break;
		case "cuci.udg.mx":
			mailServer = 'correo.cuci.udg.mx:80';
			validDomain = true;
			break;
		case "cucea.udg.mx":
			mailServer = 'wendy.cucea.udg.mx:80';
			validDomain = true;
			break;
		case "red.radio.udg.mx":
			mailServer = 'red.radio.udg.mx:81';
			validDomain = true;
			break;
		case "cucei.udg.mx":/*nose*/
			mailServer	= "https://"+ value_username + ":" + value_password +"cucei.udg.edu.mx/exchange";
			validDomain	= true;
			https		= true;
			break;
		case "csh.udg.mx":/*nose*/
			mailServer	= "https://"+ value_username + ":" + value_password +"cucsh.udg.edu.mx/exchange";
			validDomain	= true;
			https		= true;
			break;
		case "cucsur.udg.mx":
			mailServer	= "https://"+ value_username + ":" + value_password +"cucsur.udg.edu.mx/exchange";
			validDomain	= true;
			https		= true;
			break;
		case "cusur.udg.mx":
			mailServer	= "https://"+ value_username + ":" + value_password +"cusur.udg.edu.mx/exchange";
			validDomain	= true;
			https		= true;
			break;
		case "cuvalles.udg.mx":
			mailServer	= "https://"+ value_username + ":" + value_password +"cuvalles.udg.edu.mx/exchange";
			validDomain	= true;
			https		= true;
			break;
		case "cepe.udg.mx":
			mailServer	= "https://"+ value_username + ":" + value_password +"cepe.udg.edu.mx/exchange";
			validDomain	= true;
			https		= true;
			break;
		case "proulex.udg.mx":
			mailServer	= "https://"+ value_username + ":" + value_password +"proulex.udg.edu.mx/exchange";
			validDomain	= true;
			https		= true;
			break;
		case "sems.udg.mx":
			mailServer	= "https://"+ value_username + ":" + value_password +"sems.udg.edu.mx/exchange";
			validDomain	= true;
			https		= true;
			break;
			
		case "redudg.udg.mx":
			mailServer	= 'https://correo.udg.mx/exchweb/bin/auth/owaauth.dll';
			validDomain	= true;
			https		= true;
			break;
		case "cucs.udg.mx":
			mailServer	= 'https://correo.udg.mx/exchweb/bin/auth/owaauth.dll';
			validDomain	= true;
			https		= true;
			break;
		case "correo.udg.mx":
			mailServer	= 'https://correo.udg.mx/exchweb/bin/auth/owaauth.dll';
			validDomain	= true;
			https		= true;
			break;
		default:
			mailServer='webmail.udg.mx:80';
			break;
	}
	if (validDomain)
	{
		if(https)
		{
			document.emailForm.action	= mailServer;
			return true;
		}
		else
		{
			var targetAction = "http://" + mailServer + "/amserver/UI/Login?goto=http://" + mailServer + "/uwc/&gotoOnFail=http://" + mailServer + "/uwc/?err=1&module=LDAP" + "&org=" + domainName;
			document.emailForm.action = targetAction;
			document.getElementById('email-field').value = username;
			return true;
		}
	}
	else{
		return false;
	}
};


// Funci�n que sirve para determinar a qu� instancia de SIIAU se intenta accesar
// En base a esto, hace la redirecci�n al servidor correspondiente
var handleEmailSIIAU	= function()
{
	var tipo = document.getElementById('siiau-tipo').value;
	document.getElementById('p_clave_c').value	= document.getElementById('siiau-psw').value;;
	document.getElementById('p_codigo_c').value	= document.getElementById('UserNameSIIAU').value;;
	switch(tipo){
		case "escolar":
			document.detect.action = 'http://s2.siiau.udg.mx/wal/gupprincipal.valida_inicio';
			break;
		case "web":
			document.detect.action = 'http://siau4.siiau.udg.mx/siiaun/Login.jsp';
			break;
		case "web2005":
			document.detect.action = 'http://siau0.siiau.udg.mx/SIIAU8/SIIAU8/Login';
			break;
		case "rh":
			document.detect.action = 'http://s2.siiau.udg.mx/wus/gupprincipal.valida_inicio';
			break;
	}
	return true;
};


