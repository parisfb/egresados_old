<?php

	// Función para prevenir que robots informáticos tomen las direcciones de correo electrónico del sitio
	function proteger_correo( &$contenido )
	{
		if ( preg_match( '/UDG_CORREOS/i', $contenido ) ) 
		{
			while( preg_match( '/{UDG_CORREOS}(.*){\/UDG_CORREOS}/iusU', $contenido, $found ) )
			{
				preg_match( '/{CORREO}(.*){\/CORREO}/iusU', $found[1], $CORREO );
				preg_match( '/{TITULO}(.*){\/TITULO}/iusU', $found[1], $TITULO );
				$idcorreo	= 'udg_correo_anti_' . str_replace( ' ', '', str_replace( '.', '', str_replace( '@', '', $CORREO[1] ) ) );
				$contenidoenncriptado	= str_replace( '@', '|', $CORREO[1] );
				$html = '<div id="' . $idcorreo . '" class="udg_css_correo">email</div>
				<script>
					/*<![CDATA[*/
					$(window).load( function () {
						var texto_' . $idcorreo . '	= "' . $contenidoenncriptado . '";
						texto_' . $idcorreo . '		= texto_' . $idcorreo . '.replace(/\|/gi, "@");
						document.getElementById("' . $idcorreo . '").innerHTML	=\'<a href="mailto:\'+texto_' . $idcorreo . '+\'?subject=' . $TITULO[1] . '">\' + texto_' . $idcorreo . ' + \'</a>\';
					} );
					/*]]>*/
				</script>';
				$contenido = str_replace( $found, $html, $contenido );
			}
		}
	}

?>