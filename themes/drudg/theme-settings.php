<?php
/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * OPCIONES DEL TEMA
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 *
 * Carga y sobreescribe las opciones caracter�sticas del tema DrUDG.
 * Las opciones que no se sobreescriben son heredadas desde el tema Zen.
 *
 */

// Incluye las definiciones de zen_settings() y de zen_theme_get_default_settings().
include_once './' . drupal_get_path('theme', 'zen') . '/theme-settings.php';


/**
 * Implementaci�n de la funci�n THEMEHOOK_settings().
 *
 * @param $saved_settings
 *   Arreglo de las opciones guardadas para el tema.
 * @return
 *   Arreglo con formulario.
 */
function drudg_settings( $saved_settings ) {

  // Obtener los valores por default desde el archivo .info
  $defaults = zen_theme_get_default_settings( 'drudg' );

  // Mezcla las variables guardadas y sus valores por default
  $settings = array_merge( $defaults, $saved_settings );

  /*
   * Crea el formulario usando la API para Formularios de Drupal: http://api.drupal.org/api/6
   */
  $form = array();
  /* -- Borre esta l�nea si desea usar esta opci�n
  $form['drudg_example'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use this sample setting'),
    '#default_value' => $settings['drudg_example'],
    '#description'   => t( "Esta opci�n no hace nada; es s�lo un ejemplo." ),
  );
  // */

  // Agregar las opciones bases del tema.
  $form += zen_settings( $saved_settings, $defaults );

  // Remover algunas de las opciones base del tema.
  unset( $form['themedev']['zen_layout'] ); // No es necesario seleccionar la hoja de estilo base.

  // Regresar el formulario
  return $form;
}
