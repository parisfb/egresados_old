<?php
/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * TEMPLATE PARA COMENTARIOS
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 *
 * Todos los comentarios del tema se contruyen en base a este archivo.
 *
 */

/**
 * @file comment.tpl.php
 * Implementaci�n por default del tema para comentarios.
 *
 * Variables disponibles:
 * - $author: Autor del comentario. Puede ser un link o texto plano.
 * - $classes: Conjunto de clases CSS para el DIV que envuelve al comentario.
     Posibles valores: comment, comment-new, comment-preview,
     comment-unpublished, comment-published, odd, even, first, last,
     comment-by-anon, comment-by-author, � comment-mine.
 * - $content: Cuerpo del mensaje.
 * - $date: D�a y hora del mensaje.
 * - $links: Diversos links operacionales.
 * - $new: Etiqueta de 'Nuevo comentario'.
 * - $picture: Imagen del autor.
 * - $signature: Firma del autor.
 * - $status: Estado del comentario. Los posibles valores son:
 *   comment-unpublished, comment-published � comment-preview.
 * - $submitted: Cuando fue subido el comentario, con fecha y hora.
 * - $title: T�tulo con link.
 * - $unpublished: Variable para saber si el comentario est� sin publicar
 *
 * Estas dos variables est�n prove�das para el contexto.
 * - $comment: Objeto completo del comentario.
 * - $node: Objeto nodo al que el comentario est� adjunto.
 *
 * @see template_preprocess_comment()
 * @see theme_comment()
 */
?>

<div class="<?php print $classes; ?>">
  <div class="comment-inner clear-block">
    <div class="block_ctl">
      <div class="block_ctr">
        <div class="block_cbr">
          <div class="block_cbl">
            <?php if ($title): ?>
            <h3 class="title"> <?php print $title; ?>
              <?php if ($comment->new): ?>
              <span class="new"><?php print $new; ?></span>
              <?php endif; ?>
            </h3>
            <?php elseif ($comment->new): ?>
            <div class="new"><?php print $new; ?></div>
            <?php endif; ?>
            <?php if ($unpublished): ?>
            <div class="unpublished"><?php print t('Unpublished'); ?></div>
            <?php endif; ?>
            <?php if ($picture) print $picture; ?>
            <div class="submitted"> <?php print $submitted; ?> </div>
            <div class="content"> <?php print $content; ?>
              <?php if ($signature): ?>
              <div class="user-signature clear-block"> <?php print $signature; ?> </div>
              <?php endif; ?>
            </div>
            <?php if ($links): ?>
            <div class="links"> <?php print $links; ?> </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /comment-inner, /comment -->
