<?php
/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * INDEX DEL SITIO
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 *
 */

/**
 * @file index.php
 * Es la p�gina PHP que prove� todas las peticiones de p�gina en una instalaci�n de Drupal.
 *
 * Las rutinas aqu� asignan el control al manejador apropiado el que lo imprime en la p�gina apropiada.
 *
 * Todo el c�digo de Drupal est� regido bajo la GNU General Public License.
 * Ver COPYRIGHT.txt y LICENSE.txt de Drupal.
 */

require_once './includes/bootstrap.inc';
require_once './themes/drudg/funciones_udg.php';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$return = menu_execute_active_handler();

// Las constantes del 'Menu status' son enteros; el contenido de la p�gina es una cadena.
if (is_int($return)) {
  switch ($return) {
    case MENU_NOT_FOUND:
      drupal_not_found();
      break;
    case MENU_ACCESS_DENIED:
      drupal_access_denied();
      break;
    case MENU_SITE_OFFLINE:
      drupal_site_offline();
      break;
  }
}
elseif (isset($return)) {
	// Imprime cualquier valor (incluyendo una cadena vac�a) excepto NULL � undefined.
	print theme('page', $return);
}
drupal_page_footer();