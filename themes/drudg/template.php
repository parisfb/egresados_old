<?php
/*
 * UNIVERSIDAD DE GUADALAJARA
 * Modelo web para el desarrollo de sitios web universitarios
 *
 * DrUDG 0.13
 *
 * TEMPLATE DEL TEMA
 * Por: Jorge Ramos y Genaro Ram�rez
 * Mayo 2010
 */

/**
 * @file template.php
 * Contiene las funciones sobreescritas y de preproceso para el tema.
 *
 * ACERCA DEL ARCHIVO TEMPLATE.PHP
 *
 *   El archivo template.php es uno de los archivos m�s usados cuando se crean
 *   o modifican temas de Drupal. Usted puede agregar nuevas regiones para contenido de bloques,
 *   modificar o sobreescribir funciones del tema de Drupal, interceptar o hacer variables adicionales
 *   que est�n disponibles para el tema y crear c�digo PHP personalizado.variables available to your theme, and create custom PHP logic.
 *   Para m�s informaci�n, s�rvase de visitar la Gu�a para Desarrolladores de Temas en Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * SOBREESCRIBIR FUNCIONES DEL TEMA
 *
 *   El sistema de temas de Drupal usa funciones especiales de temas para generar salidas HTML
 *   automaticamente. Com�nmente es deseable personalizar esta salida en HTML. Para lograrlo
 *   tenemos que sobreescribir las funciones del tema. Primeramente, usted debe encontrar
 *   la funci�n que genera la salida y entonces sobreescribirla y modificarla en este archivo template.php.
 *   LA manera m�s f�cil de hacerlo es copiar entera la funci�n original y pegarla aqu�,
 *   cambiando el prefijo de la funci�n de theme_ to drudg_.
 *   Por ejemplo: 
 *
 *     original: theme_breadcrumb()
 *     sobreescrita: drudg_breadcrumb()
 *
 *   donde 'drudg' es el nombre del subtema (en este caso DrUDG).
 *
 *   Si usted quisiera sobreescribir alguna de las funciones del tema usadas en el n�cleo del tema Zen,
 *   usted deber�a primero observar en c�mo el n�cleo del tema Zen implementa esas funciones:
 *     theme_breadcrumbs()      en zen/template.php
 *     theme_menu_item_link()   en zen/template.php
 *     theme_menu_local_tasks() en zen/template.php
 *
 *   Para m�s informaci�n, s�rvase visitar la Gu�a del Desarrollador de Temas en:
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREAR O MODIFICAR VARIABLES PARA SU TEMA
 *
 *   Cada archivo de template tpl.php cuenta con algunas variables que toman varias piezas de contenido.
 *   Usted ouede modificar esas variables (o agregar nuevas) antes de que las mismas 
 *   sean usadas en los archivos de template usando funciones de preproceso.
 *
 *   Esto hace qye las funciones de THEME_preprocess_HOOK() las m�s poderosas para los
 *   desarrolladores de temas.
 *
 *  Esto funciona teniendo una funci�n de preproceso por cada archivo de template
 *  o sus derivados (llamados templates de sugerencias). Por ejemplo:
 *     THEME_preprocess_page    altera las variables para page.tpl.php
 *     THEME_preprocess_node    altera las variables para node.tpl.php �
 *                              para node-forum.tpl.php
 *     THEME_preprocess_comment altera las variables para comment.tpl.php
 *     THEME_preprocess_block   altera las variables para block.tpl.php
 *
 *   Para m�s informaci�n de funciones de preproceso, s�rvase de visitar
 *   la Gu�a de Desarrolladores de Temas en Drupal.org: http://drupal.org/node/223430
 *   Para m�s informaci�n sobre templates de sugerencuas, s�rvase en visitar
 *   la Gu�a de Desarrolladores de Drupal en Drupal.org: http://drupal.org/node/223440 y
 *   http://drupal.org/node/190815#template-suggestions
 */


/*
 * Agregue cualquier hoja de estilo que necesite para este subtema.
 *
 * Para agregar hojas de estilo que SIEMPRE necesiten ser inclu�das, usted deber�a agregarlas 
 * dentro de su archivo .info. Solamente use esta secci�n si est� incluyendo
 * hojas de estilo basadas en ciertas condiciones.
 */
/* -- Borre esta l�nea si usted desea usar y modificar este c�digo
// Example: Opcionalmente agregue un archivo personalizado de CSS.
if (theme_get_setting('drudg_fixed')) {
  drupal_add_css(path_to_theme() . '/layout-fixed.css', 'theme', 'all');
}
// */




/**
 * Implementaci�n de HOOK_theme().
 */
function drudg_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Agregue su hook personalizado de tema como sigue:
  /*
  $hooks['hook_name_here'] = array( // Detalles van aqu� );
  */
  return $hooks;
}



/**
 * Sobreescribir el �cono para los feeds
 *
 * @param $url
 *   LA URL del feed
 */
function drudg_feed_icon($url) {
  $icon_url = '/' . drupal_get_path( 'theme', 'drudg')  . '/images/feed.png';
  if( $image = '<img src="' . $icon_url . '" alt="' . t( 'RSS' ) . '" />' ) {
    return '<a class="xml-icon" href="' . check_url( $url ) . '">' . $image . '</a>';
  }
}



/**
 * Sobreescribir o insertar variables dentro de todos los templates
 *
 * @param $vars
 *   Un arreglo de variables a pasar al template del tema.
 * @param $hook
 *   El nombre del template para ser interpretado (nombre del archivo .tpl.php).
 */
/* -- Borre esta l�nea si desea utilizar esta funci�n
function drudg_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */


/**
 * Sobreescribir o insertar variables dentro de todos los templates
 *
 * @param $vars
 *   Un arreglo de variables a pasar al template del tema.
 * @param $hook
 *   El nombre del template para ser interpretado (nombre del archivo .tpl.php).
 */
function phptemplate_tinymce_theme($init, $textarea_name, $theme_name, $is_running) {
  static $access, $integrated;

  if (!isset($access)) {
    $access = function_exists('imce_access') && imce_access();
  }

  $init = theme_tinymce_theme($init, $textarea_name, $theme_name, $is_running);

  if ($init && $access) {
    $init['file_browser_callback'] = 'imceImageBrowser';
    if (!isset($integrated)) {
      $integrated = TRUE;
      drupal_add_js("function imceImageBrowser(fid, url, type, win) {win.open(Drupal.settings.basePath +'?q=imce&app=TinyMCE|url@'+ fid, '', 'width=760,height=560,resizable=1');}", 'inline');
    }
  }

  return $init;
}



// Funci�n para crear la lista de elementos de un men�
function phptemplate_menu_item( $link, $has_children, $menu = '', $in_active_trail = true, $extra_class = NULL )
{
	$class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
	if (!empty($extra_class)) 
	{
		$class .= ' '. $extra_class;
	}
	if ($in_active_trail) 
	{
		$class .= ' active-trail';
	}
	$css_class = phptemplate_id_safe(str_replace(' ', '_', strip_tags($link)));
	return '<li class="'. $class . ' ' . $css_class . '">' . $link . $menu ."</li>\n";
}

// Funci�n para crear un id que sea seguro y sin caracteres raros
function phptemplate_id_safe($string)
{
  // Remplaza con guiones cualquier caracter diferente a A-Z, n�meros, guiones, � guiones bajos.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // Si el primer caracter no es a-z, agregar una 'n' al principio
  if (!ctype_lower($string{0})) { // No usar ctype_alpha.
	$string = 'id'. $string;
  }
  return $string;
}



// Funci�n para crear un men� basado en el tema Zen
function generar_menu_zen($link, $has_children, $menu = '', $in_active_trail = true, $extra_class = NULL,$template_files)
{
	$class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
	if (!empty($extra_class)) {
		$class .= ' '. $extra_class;
	}
	if ($in_active_trail) {
		$class .= ' active-trail';
	}
	$css_class = phptemplate_id_safe(str_replace(' ', '_', strip_tags($link)));
	
	$Hijos				= "";
	if(is_array($template_files))
	{
		foreach ($template_files as $numero_hijo=>$array_hijo)
		{
			$Hijos		.= "<ul>" . generar_menu_zen($array_hijo["link"], $array_hijo["has_children"], $array_hijo["menu"], "" , "",$array_hijo["template_files"])  . "</ul>";			
		}
	}
	return '<li class="'. $class . ' ' . $css_class . '">' . $link . $menu ."</li>\n";
}





// Cargar archivo de funciones personalizadas
require_once( "programacion/funciones_udg.php" );



// Cargar archivos personalizados
drupal_add_js( path_to_theme() . '/udg-drupal-login.js' );
drupal_add_js( path_to_theme() . '/udg_slideshow.js' );
drupal_add_css( path_to_theme() . '/local.css', 'theme', 'all' );



/**
 * Funci�n para sobreescribir o insertar variables dentro de los templates de p�ginas.
 *
 * @param $vars
 *   Arreglo de variables a pasar al template del tema.
 * @param $hook
 *   El nombre del template para ser interpretado ("page" en este caso.)
 */
/* -- Borre esta l�nea si desea utilizar esta funci�n
function drudg_preprocess_page(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Funci�n para sobreescribir o insertar variables dentro de los templates de nodos.
 *
 * @param $vars
 *    Arreglo de variables a pasar al template del tema.
 * @param $hook
 *   El nombre del template para ser interpretado ("node" en este caso.)
 */
/* -- Borre esta l�nea si desea utilizar esta funci�n
function drudg_preprocess_node(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Funci�n para sobreescribir o insertar variables dentro de los templates de comentarios.
 *
 * @param $vars
 *   Arreglo de variables a pasar al template del tema.
 * @param $hook
 *   El nombre del template para ser interpretado ("comment" en este caso.)
 */
/* -- Borre esta l�nea si desea utilizar esta funci�n
function drudg_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Funci�n para sobreescribir o insertar variables dentro de los templates de bloques.
 *
 * @param $vars
 *   Arreglo de variables a pasar al template del tema.
 * @param $hook
 *   El nombre del template para ser interpretado ("block" en este caso.)
 */
/* -- Borre esta l�nea si desea utilizar esta funci�n
function drudg_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

